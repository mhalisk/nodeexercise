const axios = require('axios');

async function callNextUrl(nextUrl, arr) {
  try {
    const res = await axios.get(nextUrl);
    const { data } = res;
    const newArray = arr.concat(data.results);

    if (data.next) {
      return callNextUrl(data.next, newArray);
    }

    return newArray;
  } catch (err) {
    console.error(err);
    throw err;
  }
}

const sorter = query => (a, b) => {
  // Get rid of those commas
  let A = a[query].replace(/,/g, ''); // sortBy === 'name' || 'mass' || 'height'
  let B = b[query].replace(/,/g, ''); // sortBy === 'name' || 'mass' || 'height'
  // Make sure we are working with numbers, ints because no floats
  if (!isNaN(parseInt(A))) {
    A = parseInt(A);
  }
  if (!isNaN(parseInt(B))) {
    B = parseInt(B);
  }

  // Read the JS sort() docs and do the sorting
  if (A !== 'unknown') {
    // A is known
    if (B !== 'unknown') {
      // A && B are known
      return A > B ? 1 : A === B ? 0 : -1;
    }

    // Because B is unknown we want
    // A to be closer to the start and B to be later
    return -1;
  } else if (B !== 'unknown') {
    // A === 'unknown'
    return 1;
  }

  // A && B are unknown
  return 0;
};

module.exports = { callNextUrl, sorter };
