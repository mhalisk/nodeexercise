const express = require("express");
const app = express();
const axios = require("axios");
const utils = require("./utils/utils");

app.get("/people", async (req, res, next) => {
  try {
    const response = await axios.get("https://swapi.dev/api/people");
    const { data } = response;
    const { query } = req;

    let results = await utils.callNextUrl(data.next, data.results);

    if (["name", "mass", "height"].includes(query.sortBy)) {
      results = results.sort(utils.sorter(query.sortBy));
    }

    if (results) {
      res.status(200).send(results);
    }
  } catch (err) {
    res.status(400).send({ error: err });
  }
});

app.get("/planets", async (req, res, next) => {
  try {
    const response = await axios.get("https://swapi.dev/api/planets");
    const planets = await utils.callNextUrl(
      response.data.next,
      response.data.results
    );

    for (let planet of planets) {
      for (let i in planet.residents) {
        const url = planet.residents[i];
        const res = await axios.get(url);
        const { name } = res.data;
        planet.residents[i] = name;
      }
    }

    res.status(200).send(planets);
  } catch (err) {
    console.error(err);
  }
});

const server = app.listen(3000, () => {
  const host = server.address().address,
    port = server.address().port;

  console.log("API listening at http://%s:%s", host, port);
});
